<?php

class View {

    private $viewName;
    private $viewData;

    public function __construct($viewName) {
        $this->viewName = $viewName;
    }

    public function setData($data) {
        $this->viewData = $data;
    }

    public function show() {
        include __DIR__ . "/" . $this->viewName . ".view";
    }
}

?>