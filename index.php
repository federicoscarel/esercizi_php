<?php

require_once __DIR__ . '/views/view.php';
require_once __DIR__ . '/models/artistiModel.php';


// model e view usate
$view = new View('artisti');
$model = new ArtistiModel();

// funzione di utilità per sanificare l'input esterno (evitare input nocivi)
function safeInput($var) {
    return htmlspecialchars(stripslashes(trim($var)));
}

// Gestione variabili inviate

$nome = "";
$cognome = "";
$nomeArte = "";
$id = "";

$action = 'index'; // azione di default = visualizza la pagina

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (isset($_POST["addAction"])) {
        $action = "add";

        $nome = safeInput($_POST["nameField"]);
        $cognome = safeInput($_POST["cognomeField"]);
        $nomeArte = safeInput($_POST["nomeArteField"]);

    } else if (isset($_POST["modifyAction"])) {
        $id = safeInput($_POST["idSelected"]);
        $nome = safeInput($_POST["nameField"]);
        $cognome = safeInput($_POST["cognomeField"]);
        $nomeArte = safeInput($_POST["nomeArteField"]);

        $action = "modify";
    } else if (isset($_POST["deleteAction"])) {
        $action = "delete";

        $id = safeInput($_POST["idSelected"]);
    } else if (isset($_POST["showModifyAction"])) {
        $action = "showModify";

        $id = safeInput($_POST["idSelected"]);
        $view = new View('artisti-single'); // cambio la view usata
    }

}


// gestione delle azioni da eseguire
switch ($action) {
    case 'index':
        $data = $model->getAll();
        $view->setData($data);
        $view->show();
        break;
    case 'add':
        $model->add($nome, $cognome, $nomeArte);
        $data = $model->getAll();
        $view->setData($data);
        $view->show();
        break;
    case 'delete':
        $model->delete($id);
        $data = $model->getAll();
        $view->setData($data);
        $view->show();
        break;
    case 'showModify':
        $data = $model->getSingle($id);
        $view->setData($data);
        $view->show();
        break;
    case 'modify':
        $model->update($id, $nome, $cognome, $nomeArte);
        $data = $model->getAll();
        $view->setData($data);
        $view->show();
        break;
    default:
        $data = $model->getAll();
        $view->setData($data);
        $view->show();
}

?>