<?php


require_once __DIR__ . "/baseModel.php";

class ArtistiModel extends BaseModel {

    public function getAll() {
        $query = "select id, nome, cognome, nome_arte from artisti order by cognome asc, nome asc";

        $result = array();

        foreach ($this->connection->query($query) as $row) {
            array_push($result, $row);
        }

        return $result;
    }

    public function getSingle($id) {
        $query = "select id, nome, cognome, nome_arte from artisti where id = :id";

        $data = [
            'id' => $id
        ];

        $stmt = $this->connection->prepare($query);
        $stmt->execute($data);
        return $stmt->fetch();
    }

    public function add($nome, $cognome, $nomeArte) {
        $statement = "insert into artisti (nome, cognome, nome_arte) values (:nome, :cognome, :nomeArte)";

        $data = [
            'nome' => $nome,
            'cognome' => $cognome,
            'nomeArte' => $nomeArte
        ];

        // la query va prima preparata, poi si associano i dati alle variabili dichiarate e si esegue
        $this->connection->prepare($statement)->execute($data);
    }

    public function update($id, $nome, $cognome, $nomeArte) {
        $statement = "update artisti set nome = :nome, cognome = :cognome, nome_arte = :nomeArte where id = :id";

        $data = [
            'id' => $id,
            'nome' => $nome,
            'cognome' => $cognome,
            'nomeArte' => $nomeArte
        ];

        $this->connection->prepare($statement)->execute($data);
    }

    public function delete($id) {
        $statement = "delete from artisti where id = :id";

        $data = [
            'id' => $id
        ];

        $this->connection->prepare($statement)->execute($data);
    }
}

?>