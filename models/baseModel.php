<?php

require_once __DIR__ . '/../config/config.inc.php';

abstract class BaseModel {
    protected $connection;

    public function __construct() {
        try {
            $user = $GLOBALS['config']['database']['user'];
            $password = $GLOBALS['config']['database']['password'];
            $host = $GLOBALS['config']['database']['host'];
            $dbname = $GLOBALS['config']['database']['dbName'];

            $this->connection = new PDO("mysql:host=$host;dbname=$dbname", $user, $password);
            // set the PDO error mode to exception
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            //echo "Connected successfully";
        } catch(PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    abstract public function getAll();
}

?>