-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Mar 14, 2019 alle 22:31
-- Versione del server: 10.1.28-MariaDB
-- Versione PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `artisti`
--

CREATE TABLE `artisti` (
  `id` int(11) NOT NULL,
  `cognome` varchar(100) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `nome_arte` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `artisti`
--

INSERT INTO `artisti` (`id`, `cognome`, `nome`, `nome_arte`) VALUES
(1, 'Rossi', 'Mario', 'Marione!!'),
(3, 'Bianchi', 'Luciano', 'Er canaro'),
(4, 'Pravettoni', 'Carlo', 'Carcarlo');

-- --------------------------------------------------------

--
-- Struttura della tabella `collezione`
--

CREATE TABLE `collezione` (
  `nome` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `collezione`
--

INSERT INTO `collezione` (`nome`) VALUES
('Collezione 2'),
('Collezione A');

-- --------------------------------------------------------

--
-- Struttura della tabella `opere`
--

CREATE TABLE `opere` (
  `codice` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `anno` int(11) DEFAULT NULL,
  `collezione` varchar(100) DEFAULT NULL,
  `artista` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `opere`
--

INSERT INTO `opere` (`codice`, `nome`, `anno`, `collezione`, `artista`) VALUES
(1, 'Batman e Robin', 1965, 'Collezione A', 3),
(2, 'Pippo e Pluto', 2000, NULL, 1);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `artisti`
--
ALTER TABLE `artisti`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `collezione`
--
ALTER TABLE `collezione`
  ADD PRIMARY KEY (`nome`);

--
-- Indici per le tabelle `opere`
--
ALTER TABLE `opere`
  ADD PRIMARY KEY (`codice`),
  ADD KEY `collezione` (`collezione`),
  ADD KEY `artista` (`artista`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `artisti`
--
ALTER TABLE `artisti`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT per la tabella `opere`
--
ALTER TABLE `opere`
  MODIFY `codice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `opere`
--
ALTER TABLE `opere`
  ADD CONSTRAINT `opere_ibfk_1` FOREIGN KEY (`collezione`) REFERENCES `collezione` (`nome`),
  ADD CONSTRAINT `opere_ibfk_2` FOREIGN KEY (`artista`) REFERENCES `artisti` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
